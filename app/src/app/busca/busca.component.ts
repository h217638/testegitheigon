import { Component} from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from "../data.service";

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.css']
})
export class BuscaComponent {

  public subscription: Subscription;
  public nome : string ;
  public ehfilme : boolean;


  constructor(private data: DataService) {
    this.ehfilme = true;
    this.nome = "";
    this.subscription = this.data.currentMessage.subscribe(nome => this.nome = nome);
  }

  newMessage() {
    this.data.changeMessage(this.nome);
  }

  getNome(){
    return this.nome;
  }

  getBusca(){
    return this.ehfilme;
  }

  changeBusca()
  {
    if (this.ehfilme) {
      this.ehfilme = false;
    }
    else{
      this.ehfilme = true;
    }
  }

  
}


