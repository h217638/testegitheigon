import { Component, Input} from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { DataService } from "../data.service";
import { Subscription } from 'rxjs';
import { BuscaComponent } from '../busca/busca.component';

export interface Imidia { title: string; age: string; disney:number; hulu:number; primevideo:number; netflix:number; imdb:number; rotten:string; year:number; genres:string ; type:number}


@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.component.html',
  styleUrls: ['./resultado.component.css']
})
export class ResultadoComponent  {

  private itemDoc: AngularFirestoreDocument<Imidia>;

  public item: Observable<any>;
  private obj: BuscaComponent;
  private estado: boolean;
  private nome:string;
  private nomeDoc: string;
  @Input() ehfilme : boolean;
  public subscription: Subscription;

  constructor(firestore: AngularFirestore,private data: DataService) {
    this.estado = false;
    this.obj = new BuscaComponent(data);
    this.ehfilme = true;
    this.nome = this.obj.getNome();
    this.nomeDoc = 'movies/nada';
    this.itemDoc = firestore.doc<Imidia>(this.nomeDoc);
    this.item = this.itemDoc.valueChanges();
    this.subscription = this.data.currentMessage.subscribe(nome => this.update(nome,firestore));
  }

  update(novonome:string, firestore: AngularFirestore) {
    this.nome = novonome;
    if (this.ehfilme) {
      this.acessaFirestore(firestore,'movies/');
    }
    else{
      this.acessaFirestore(firestore,'series/');
    }
  }

  acessaFirestore(firestore: AngularFirestore, colecao : string)
  {
    this.nomeDoc = colecao + this.nome.toUpperCase();
    if (this.nome =="" || this.nome ==".") {
      this.nomeDoc= colecao + 'nada';
    }
    this.itemDoc = firestore.doc<Imidia>(this.nomeDoc);
    this.item = this.itemDoc.valueChanges();
  }
  getNome(){
    return this.nome;
  }
  getEstado(){
    return this.estado;
  }
  estadoTrue(){
    this.estado=true;
  }
  estadoFalse(){
    this.estado=false;
  }
  getNomeDOC(){
    return this.nomeDoc;
  }
}
